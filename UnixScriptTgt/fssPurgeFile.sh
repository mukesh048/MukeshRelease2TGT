#!/usr/bin/ksh

File=$1
Days=$2

find $File -mtime +$2 -type f -exec rm {} \;
    if [ $? -ne 0 ] 
      then
    echo " Unable to Purge File"
       exit 1 
    fi
   echo " File Purged Successfully"
    exit 0
