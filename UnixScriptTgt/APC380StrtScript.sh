#!/usr/bin/ksh


ZIP_FILEPATTERN='*.zip'

#UNZIP files present in APC Temp Folder.
cd $1
Files=`ls $1/$ZIP_FILEPATTERN`
for File in $Files
 do
    cmd=`unzip -o $File`
    if [ $? -ne 0 ]  # Check status of unzip command
     then
       exit 1 
    fi
   cmd1=`mv $File $2`
   if [ $? -ne 0 ]  # Check status of mv command
     then
      exit 1 
   fi
 done		
 exit 0
